/*
     main.js

     This exercise contains a simple web page with basic HTML / CSS / JavaScript. The defective version will
     have several errors designed to test the diagnostic ability of a developer.

     This is the WORKING version.
 */


/* Execute module as a function when loaded */
(function() {
'use strict';

    /* JQUERY - Run the Passed function when the document is ready */
    $(document).ready(JQRY_OnDocReady);

    /**
     * Function called after the document has been loaded
     */
    function JQRY_OnDocReady()
    {
        $('#myClickButton').click(function() {
            var originalString = $('#srcString').val();

            // Do something

            showResults(originalString, 'TBD2');
        });
    }



    function showResults (res1, res2) {
        document.getElementById('out1Result').innerHTML = res1;
        document.getElementById('out2Result').innerHTML = res2;
    }

}());
